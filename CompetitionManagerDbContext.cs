﻿using CompetitionManager.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class CompetitionManagerDbContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Athlete> Athletes { get; set; }
        public DbSet<Competition> Competitions { get; set; }
        public DbSet<Team> Teams { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7584\\SQLEXPRESS;Initial Catalog=CompetitionManagerDB;Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AthleteCompetition>().HasKey(pt => new { pt.AthleteId, pt.CompetitionId });

            modelBuilder.Entity<AthleteCompetition>()
                .HasOne(ac => ac.Athlete)
                .WithMany(a => a.AthleteCompetitions)
                .HasForeignKey(ac => ac.AthleteId);

            modelBuilder.Entity<AthleteCompetition>()
                .HasOne(ac => ac.Competition)
                .WithMany(a => a.AthleteCompetitions)
                .HasForeignKey(ac => ac.CompetitionId);

            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 1, FirstName = "Stephen", LastName = "Silas" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 2, FirstName = "Mark", LastName = "Daigneault" });
            modelBuilder.Entity<Coach>().HasData(new Coach { Id = 3, FirstName = "Lindsay", LastName = "Gottlieb" });

            modelBuilder.Entity<Athlete>().HasData(new Athlete { Id = 1, FirstName = "Gregg", LastName = "Popovich", CoachId = 1 });
            modelBuilder.Entity<Athlete>().HasData(new Athlete { Id = 2, FirstName = "Jamahl", LastName = "Mosley", CoachId = 1 });

            modelBuilder.Entity<Competition>().HasData(new Competition { Id = 1, Name = "Tour de France" });
            modelBuilder.Entity<Competition>().HasData(new Competition { Id = 2, Name = "OL" });
            modelBuilder.Entity<Competition>().HasData(new Competition { Id = 3, Name = "Nordic Athletics Championships" });

            modelBuilder.Entity<Team>().HasData(new Team { Id = 1, Name = "Gneist" });
            modelBuilder.Entity<Team>().HasData(new Team { Id = 2, Name = "Fana Idrettslag", CoachId = 1});
        }
    }
}

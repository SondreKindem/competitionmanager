﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager.Models
{
    public class AthleteCompetition
    {
        public int AthleteId { get; set; }
        public Athlete Athlete { get; set; }

        public int CompetitionId {get; set;}
        public Competition Competition { get; set; }
    }
}

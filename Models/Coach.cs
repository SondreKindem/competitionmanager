﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    public class Coach
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<Athlete> Athletes { get; set; }

        public Team Team { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager.Models
{
    public class Competition
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<AthleteCompetition> AthleteCompetitions { get; set; } 
    }
}

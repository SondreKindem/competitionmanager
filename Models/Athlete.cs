﻿using CompetitionManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CompetitionManager
{
    public class Athlete
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int? CoachId { get; set; }
        public Coach Coach { get; set; }

        public List<AthleteCompetition> AthleteCompetitions { get; set; }
    }
}

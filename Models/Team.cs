﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int? CoachId { get; set; }
        public Coach Coach { get; set; }
    }
}

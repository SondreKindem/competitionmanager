﻿using CompetitionManager.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CompetitionManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            ShowMainMenu();
        }

        private static void ShowMainMenu()
        {
            while (true)
            {
                Console.WriteLine("Yo, what do you want to do?");
                Console.WriteLine("1: List coaches and athletes");
                Console.WriteLine("2: Manage coaches");
                Console.WriteLine("3: Manage athletes");
                Console.WriteLine("4: Assign coaches to athletes");
                Console.WriteLine("5: Join competition");
                Console.WriteLine("6: Assign coach to team");
                Console.WriteLine("q: Quit");

                Console.Write("> ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        PrintAll();
                        break;
                    case "2":
                        ManageCoachesMenu();
                        break;
                    case "3":
                        ManageAthletesMenu();
                        break;
                    case "4":
                        AssignCoachMenu();
                        break;
                    case "5":
                        JoinCompetitionMenu();
                        break;
                    case "6":
                        AssignCoachToTeamMenu();
                        break;
                    case "q":
                        return;
                }
            }
        }

        /*
         * Menus
         */
        #region Menus

        private static void JoinCompetitionMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Add athlete to competition!");

            PrintAllAthletes();
            Console.WriteLine();
            PrintAthletesInCompetitions();
            Console.WriteLine();

            try
            {
                Console.Write("Select athlete: ");
                int athleteId = int.Parse(Console.ReadLine());

                Console.Write("Select competition: ");
                int competitionId = int.Parse(Console.ReadLine());

                JoinCompetition(athleteId, competitionId);
            }
            catch (FormatException) { }
        }

        private static void AssignCoachToTeamMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Add coach to team! Use coach 'null' to remove coach from a team");

            PrintAllCoaches();
            Console.WriteLine();
            PrintAllTeams();

            try
            {
                Console.Write("Select coach: ");
                string input = Console.ReadLine();

                // We can feed a null value as coachId to set the team coach to null
                int? coachId;
                if (input == "null")
                    coachId = null;
                else
                    coachId = int.Parse(input);

                Console.Write("Select team: ");
                int teamId = int.Parse(Console.ReadLine());

                AssignCoachToTeam(coachId, teamId);
            }
            catch (FormatException) { }
        }

        private static void AssignCoachMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Assign coaches!");

            PrintAll();
            Console.WriteLine();
            try
            {
                Console.Write("Select athlete: ");
                int athleteId = int.Parse(Console.ReadLine());

                Console.Write("Select coach: ");
                int coachId = int.Parse(Console.ReadLine());

                AssignCoach(athleteId, coachId);
            }
            catch (FormatException) { }
        }

        private static void ManageCoachesMenu()
        {
            Console.WriteLine();

            PrintAll();

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Coach management");
                Console.WriteLine("n: Add new coach");
                Console.WriteLine("d: Delete coach");
                Console.WriteLine("b: Back");

                Console.Write("> ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "n":
                        AddCoachMenu();
                        break;
                    case "d":
                        DeleteCoachMenu();
                        break;
                    case "b":
                        return;
                }
            }
        }

        private static void ManageAthletesMenu()
        {
            Console.WriteLine();

            PrintAllAthletes();

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Athlete management");
                Console.WriteLine("n: Add new athlete");
                Console.WriteLine("d: Delete athlete");
                Console.WriteLine("b: Back");

                Console.Write("> ");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "n":
                        AddAthleteMenu();
                        break;
                    case "d":
                        DeleteAthleteMenu();
                        break;
                    case "b":
                        return;
                }
            }
        }

        private static void DeleteCoachMenu()
        {
            PrintAll();

            while (true)
            {
                Console.WriteLine("Select a coach or type EXIT");
                Console.Write("Coach id: ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    break;

                try
                {
                    int coachId = int.Parse(input);

                    DeleteCoach(coachId);
                    break;
                }
                catch (FormatException) { }
            }
        }

        private static void DeleteAthleteMenu()
        {
            PrintAllAthletes();

            while (true)
            {
                Console.WriteLine("Select an athlete or type EXIT");
                Console.Write("Athlete id: ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    break;

                try
                {
                    int athleteId = int.Parse(input);

                    DeleteAthlete(athleteId);
                    break;
                }
                catch (FormatException) { }
            }
        }

        private static void AddCoachMenu()
        {
            Console.WriteLine("Adding new coach. Type EXIT to stop");
            string firstName;
            string lastName;

            // Keep querying for input until input is valid
            while (true)
            {
                Console.Write("First name: ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    return;

                if (!string.IsNullOrWhiteSpace(input))
                {
                    firstName = input;
                    break;
                }
            }
            while (true)
            {
                Console.Write("Last name: ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    return;

                if (!string.IsNullOrWhiteSpace(input))
                {
                    lastName = input;
                    break;
                }
            }
            AddCoach(new Coach() { FirstName = firstName, LastName = lastName });
        }

        private static void AddAthleteMenu()
        {
            Console.WriteLine("Adding new athlete. Type EXIT to stop");

            string firstName;
            string lastName;
            int coachId = 0;

            while (true)
            {
                Console.Write("First name: ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    return;

                if (!string.IsNullOrWhiteSpace(input))
                {
                    firstName = input;
                    break;
                }
            }

            while (true)
            {
                Console.Write("Last name: ");
                string input = Console.ReadLine();

                if(input == "EXIT")
                    return;

                if (!string.IsNullOrWhiteSpace(input))
                {
                    lastName = input;
                    break;
                }
            }

            while (true)
            {
                Console.Write("Coach id (optional): ");
                string input = Console.ReadLine();

                if (input == "EXIT")
                    return;
                else if (input == "")
                    break;

                if (!string.IsNullOrWhiteSpace(input))
                {
                    try
                    {
                        coachId = int.Parse(input);
                        break;
                    }
                    catch (Exception) { }
                }
            }

            if (coachId <= 0)
            {
                AddAthlete(new Athlete() { FirstName = firstName, LastName = lastName });
            }
            else
            {
                AddAthlete(new Athlete() { FirstName = firstName, LastName = lastName, CoachId = coachId });
            }
        }
        #endregion

        /*
         * DB Stuffs
         */
        #region DB stuff

        /* Inserts */

        private static void AddCoach(Coach coach)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                try
                {
                    competitionManagerDbContext.Add<Coach>(coach);
                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Added coach!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        private static void AddAthlete(Athlete athlete)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                try
                {
                    competitionManagerDbContext.Add<Athlete>(athlete);
                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Added athlete!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        private static void JoinCompetition(int athleteId, int competitionId)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                try
                {
                    competitionManagerDbContext.Add<AthleteCompetition>(new AthleteCompetition { AthleteId = athleteId, CompetitionId = competitionId });
                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Added athlete to competition!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        /* Deletes */

        // Deleting an athlete will automatically remove the relevant entries from the athleteCompetition table
        private static bool DeleteAthlete(int athleteId)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                competitionManagerDbContext.Remove<Athlete>(new Athlete() { Id = athleteId });
                try
                {
                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Athlete deleted");
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error when trying to delete: " + ex.Message);
                }
                return false;
            }
        }

        private static bool DeleteCoach(int coachId)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                // The coachId constraint for the athletes is not set to null for some reason? Good excuse to play around with some queries
                Coach coach = competitionManagerDbContext.Coaches.Include(c => c.Athletes).FirstOrDefault(c => c.Id == coachId);

                foreach (Athlete athlete in coach.Athletes)
                {
                    athlete.CoachId = null;
                }

                competitionManagerDbContext.Athletes.UpdateRange(coach.Athletes);

                // Finally delete the coach!
                competitionManagerDbContext.Remove<Coach>(coach);
                try
                {
                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Coach deleted");
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error when trying to delete " + ex.Message);
                }
            }
            return false;
        }

        /* Updates */

        private static bool AssignCoachToTeam(int? coachId, int teamId)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                try
                {
                    //var team = new Team { Id = teamId };
                    //competitionManagerDbContext.Attach(team);

                    //team.Coach = null;

                    Team team = competitionManagerDbContext.Find<Team>(teamId);
                    team.CoachId = coachId;

                    competitionManagerDbContext.Update(team);

                    competitionManagerDbContext.SaveChanges();
                    Console.WriteLine("Assigned coach to team");
                    return true;
                }
                catch(Exception ex)
                {
                    if(ex.InnerException != null)
                        Console.WriteLine(ex.InnerException.Message);
                    else
                        Console.WriteLine(ex.Message);
                }
            }
            return false;
        }

        private static bool AssignCoach(int athleteId, int coachId)
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Athlete athlete = competitionManagerDbContext.Find<Athlete>(athleteId);
                if (athlete != null)
                {
                    athlete.CoachId = coachId;
                    competitionManagerDbContext.Update(athlete);

                    try
                    {
                        competitionManagerDbContext.SaveChanges();
                        Console.WriteLine("Coach assigned to athlete!");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Could not assign coach: " + ex.Message);
                        return false;
                    }
                }

                return false;
            }
        }

        private static void PrintAll()
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Console.WriteLine("Coaches with athletes:");
                foreach(Coach coach in competitionManagerDbContext.Coaches.Include(c => c.Athletes))
                {
                    Console.WriteLine($"{coach.Id}: {coach.FirstName} {coach.LastName}");

                    // Print athletes of coach
                    if (coach.Athletes != null)
                    {
                        foreach (Athlete athlete in coach.Athletes)
                        {
                            Console.WriteLine($"\t{athlete.Id}: {athlete.FirstName} {athlete.LastName}");
                        }
                    }
                }

                Console.WriteLine("Unassigned athletes:");
                foreach (Athlete athlete in competitionManagerDbContext.Athletes.Where(a => a.CoachId == null))
                {
                    Console.Write($"{athlete.Id}: {athlete.FirstName} {athlete.LastName}");
                }
            }
        }

        private static void PrintAllAthletes()
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Console.WriteLine("All athletes:");
                foreach (Athlete athlete in competitionManagerDbContext.Athletes)
                {
                    Console.WriteLine($"{athlete.Id}: {athlete.FirstName} {athlete.LastName}");
                }
            }
        }

        private static void PrintAllCompetitions()
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Console.WriteLine("All competitions:");
                foreach (Competition competition in competitionManagerDbContext.Competitions)
                {
                    Console.WriteLine($"{competition.Id}: {competition.Name}");
                }
            }
        }

        private static void PrintAllTeams()
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Console.WriteLine("All teams:");
                foreach (Team team in competitionManagerDbContext.Teams)
                {
                    Console.WriteLine($"{team.Id}: {team.Name}, coach = {team.CoachId}");
                }
            }
        }

        private static void PrintAllCoaches()
        {
            using (CompetitionManagerDbContext competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                Console.WriteLine("All coaches:");
                foreach (Coach coach in competitionManagerDbContext.Coaches)
                {
                    Console.WriteLine($"{coach.Id}: {coach.FirstName} {coach.LastName}");
                }
            }
        }

        private static void PrintAthletesInCompetitions()
        {
            Console.WriteLine("Athletes in competitions:");
            using (var competitionManagerDbContext = new CompetitionManagerDbContext())
            {
                // Traverse the many to many relationship starting with the competition
                 foreach(Competition competition in competitionManagerDbContext.Competitions.Include(c => c.AthleteCompetitions).ThenInclude(ac => ac.Athlete))
                {
                    Console.WriteLine(competition.Id + ": " + competition.Name + ":");
                    foreach(var athleteCompetition in competition.AthleteCompetitions)
                    {
                        Console.WriteLine($"\t{athleteCompetition.Athlete.Id}: {athleteCompetition.Athlete.FirstName} {athleteCompetition.Athlete.LastName}");
                    }
                }
            }
        }
    }
    #endregion
}

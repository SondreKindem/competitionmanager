﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { 1, "Stephen", "Silas" });

            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { 2, "Mark", "Daigneault" });

            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { 3, "Lindsay", "Gottlieb" });

            migrationBuilder.InsertData(
                table: "Athlete",
                columns: new[] { "Id", "CoachId", "FirstName", "LastName" },
                values: new object[] { 1, 1, "Gregg", "Popovich" });

            migrationBuilder.InsertData(
                table: "Athlete",
                columns: new[] { "Id", "CoachId", "FirstName", "LastName" },
                values: new object[] { 2, 1, "Jamahl", "Mosley" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Athlete",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Coaches",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}

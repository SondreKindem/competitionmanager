﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CompetitionManager.Migrations
{
    public partial class AddCompAndTeamSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Coaches_CoachId",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_CoachId",
                table: "Team");

            migrationBuilder.AlterColumn<int>(
                name: "CoachId",
                table: "Team",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.InsertData(
                table: "Competition",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Tour de France" },
                    { 2, "OL" },
                    { 3, "Nordic Athletics Championships" }
                });

            migrationBuilder.InsertData(
                table: "Team",
                columns: new[] { "Id", "CoachId", "Name" },
                values: new object[,]
                {
                    { 1, null, "Gneist" },
                    { 2, 1, "Fana Idrettslag" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Team_CoachId",
                table: "Team",
                column: "CoachId",
                unique: true,
                filter: "[CoachId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Coaches_CoachId",
                table: "Team",
                column: "CoachId",
                principalTable: "Coaches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Coaches_CoachId",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_CoachId",
                table: "Team");

            migrationBuilder.DeleteData(
                table: "Competition",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Competition",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Competition",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Team",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Team",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AlterColumn<int>(
                name: "CoachId",
                table: "Team",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Team_CoachId",
                table: "Team",
                column: "CoachId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Coaches_CoachId",
                table: "Team",
                column: "CoachId",
                principalTable: "Coaches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
